# Programación y Aplicaciones para la Web #

* Semestre: Quinto Semestre
* Horas por semana: 3

## Universidad Autónoma de Nuevo León ##
### Facultad de Ciencas Físico Matemáticas ###
### Multimedia y Animación Digital ###

Repositorio para la clase de PAPW impartida en el semestre Agosto-Diciembre 2018.

### Planeación Semanal ###

* #### 7 y 9 ago: Introduccion HTML ####
* #### 14 y 16 ago: HTML y CSS ####
* #### 21 y 23 ago: Formularios HTML y control de errores con Javascript ####
* #### 28 y 30 ago: JQuery ####
* #### 4 y 6 sep: NetBeans e introducción de Java ####
* ## 11 sep: Primer examen parcial ##
* #### 13 sep: Introducción a POO de Java ####
* #### 18 y 20 sep: Java POO ####
* #### 25 y 27 sep: Base de datos relacional ####
* #### 2 y 4 oct: Operaciones con una base de datos ####
* #### 9 y 11 oct: JSP ####
* ## 16 oct: Segundo examen parcial ##
* #### 18 oct: JSTL ####
* #### 23 y 25 oct: Introducción a Servlets ####
* #### 30 oct y 1 nov: Servlets ####
* #### 6 y 8 nov: Usuarios y hosting ####
* #### 13 y 15 nov: Manejo de archivos ####
* ## Examen final por definir ##
